# Experiments for DPCP Paper: Check-in Desk Scheduling Optimisation at CDG International Airport 

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/productions-tfalque%2Farticles%2Fcheck-in-scheduling-optim-cdg-airport%2Fexperiments/main)

This repository contains the details of our campaign that is presented for our paper submitted to 
the Doctoral Program of Constraint Programming Conference 2023.

## Abstract

More than ever, air transport players (i.e., airline and airport companies) in an intensely competitive climate need to benefit from a carefully optimized management of airport resources to improve the quality of service and control the induced costs.
In this paper, we investigate the Airport Check-in Desk Assignment Problem. 
We propose a Constraint Programming (CP) model for this problem, and present some promising experimental results from data coming from ADP (Aéroport de Paris). 

## Summary

1. [Output](./output) contains all the output files (including `tex` file for the table present in the paper). 
1. [1-Load_experiments.ipynb](./1-Load_experiments.ipynb) This notebook reads the lod data files and generates a `metrics`
   cache file. 
1. [2-Analysis.ipynb](./2-Analysis.ipynb) This notebook analyzes the results and produces various figures and tables. 

## Authors 

- Thibault Falque 